//
//  FizzBuzzHelperTests.swift
//  TestProjectTests
//
//  Created by Kyle Schwarzkopf on 3/26/18.
//  Copyright © 2018 Kyle Schwarzkopf. All rights reserved.
//

import XCTest
@testable import TestProject

class FizzBuzzHelperTests: XCTestCase {
    let fizzBuzzHelper = FizzBuzzHelper()
    
    func testIsDivisibleByThree() {
        let result = fizzBuzzHelper.isDivisibleByThree(number: 3)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByThree() {
        let result = fizzBuzzHelper.isDivisibleByThree(number: 1)
        XCTAssertEqual(result, false)
    }
    
    func testIsDivisibleByFive() {
        let result = fizzBuzzHelper.isDivisibleByFive(number: 5)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByFive() {
        let result = fizzBuzzHelper.isDivisibleByFive(number: 1)
        XCTAssertEqual(result, false)
    }
    
    func testIsDivisibleByFifteen() {
        let result = fizzBuzzHelper.isDivisibleByFifteen(number: 15)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByFifteen() {
        let result = fizzBuzzHelper.isDivisibleByFifteen(number: 1)
        XCTAssertEqual(result, false)
    }
    
    func testSayFizz() {
        let result = fizzBuzzHelper.check(number: 3)
        XCTAssertEqual(result, "Fizz")
    }
    
    func testSayBuzz() {
        let result = fizzBuzzHelper.check(number: 5)
        XCTAssertEqual(result, "Buzz")
    }
    
    func testSayFizzBuzz() {
        let result = fizzBuzzHelper.check(number: 15)
        XCTAssertEqual(result, "FizzBuzz")
    }
    
    func testSayNumber() {
        let result = fizzBuzzHelper.check(number: 1)
        XCTAssertEqual(result, "1")
    }
    
    func testArrayLength() {
        let result = fizzBuzzHelper.getFizzBuzzContent(from: 3)
        XCTAssertEqual(result.count, 3)
    }
    
    func testWrongArrayLength() {
        let result = fizzBuzzHelper.getFizzBuzzContent(from: 3)
        XCTAssertFalse(result.count == 4)
    }
}
