//
//  UIViewController.swift
//  TestProject
//
//  Created by Kyle Schwarzkopf on 3/26/18.
//  Copyright © 2018 Kyle Schwarzkopf. All rights reserved.
//

import UIKit

extension UIViewController {
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

