//
//  FizzBuzzHelper.swift
//  TestProject
//
//  Created by Kyle Schwarzkopf on 3/26/18.
//  Copyright © 2018 Kyle Schwarzkopf. All rights reserved.
//

import Foundation

enum FizzBuzz: String {
    case fizz = "Fizz"
    case fizzBuzz = "FizzBuzz"
    case buzz = "Buzz"
}

struct FizzBuzzHelper {
    internal func isDivisibleByThree(number: Int) -> Bool {
        return isDivisibleBy(divisor: 3, number: number)
    }
    
    internal func isDivisibleByFive(number: Int) -> Bool {
        return isDivisibleBy(divisor: 5, number: number)
    }
    
    internal func isDivisibleByFifteen(number: Int) -> Bool {
        return isDivisibleBy(divisor: 15, number: number)
    }
    
    internal func isDivisibleBy(divisor: Int, number: Int) -> Bool {
        return number % divisor == 0
    }
    
    internal func check(number: Int) -> String {
        if isDivisibleByFifteen(number: number) {
            return FizzBuzz.fizzBuzz.rawValue
        } else if isDivisibleByThree(number: number) {
            return FizzBuzz.fizz.rawValue
        } else if isDivisibleByFive(number: number){
            return FizzBuzz.buzz.rawValue
        } else {
            return "\(number)"
        }
    }
    
    internal func getFizzBuzzContent(from number: Int) -> [String] {
        var fizzBuzzList = [String]()
        for i in 1...number {
            let fizzBuzzElement = check(number: i)
            fizzBuzzList.append(fizzBuzzElement)
        }
        
        return fizzBuzzList
    }
}
