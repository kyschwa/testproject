//
//  HomeViewController.swift
//  Taco Tracker
//
//  Created by Kyle Schwarzkopf on 3/22/18.
//  Copyright © 2018 Kyle Schwarzkopf. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController { }

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showFizzBuzz", sender: nil)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "fizz buzz test"
        return cell
    }
}
