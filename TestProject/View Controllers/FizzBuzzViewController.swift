//
//  FizzBuzzViewController.swift
//  TestProject
//
//  Created by Kyle Schwarzkopf on 3/26/18.
//  Copyright © 2018 Kyle Schwarzkopf. All rights reserved.
//

import UIKit

class FizzBuzzViewController: UIViewController {
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var fizzBuzzTableView: UITableView!
    var fizzBuzzList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberTextField.delegate =  self
    }
    
    @IBAction func GoButtonPressed(_ sender: Any) {
        if let text = numberTextField.text, !text.isEmpty {
            guard let fizzBuzzCount = Int(text) else { fatalError("could not convert text to number") }
            let fizzBuzzHelper = FizzBuzzHelper()
            fizzBuzzList = fizzBuzzHelper.getFizzBuzzContent(from: fizzBuzzCount)
            fizzBuzzTableView.reloadData()
        } else {
            alert(message: "Please enter a value")
        }
    }
    
    private func checkIfNumberIsBelow100(with number: Int) -> Bool {
        return number < 100
    }
}

extension FizzBuzzViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fizzBuzzList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text =  fizzBuzzList[indexPath.row]
        return cell
    }
}

extension FizzBuzzViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        return string == numberFiltered && updatedText.count <= 2
    }
}


